import { Button, DocumentLoadEvent, PdfJs, Position, PrimaryButton, Tooltip, Viewer, Worker } from '@react-pdf-viewer/core';
import { defaultLayoutPlugin } from '@react-pdf-viewer/default-layout';
import '@react-pdf-viewer/core/lib/styles/index.css'
import { bookmarkPlugin } from '@react-pdf-viewer/bookmark';
import { searchPlugin } from '@react-pdf-viewer/search';
import '@react-pdf-viewer/search/lib/styles/index.css';
import '@react-pdf-viewer/bookmark/lib/styles/index.css';
import '@react-pdf-viewer/core/lib/styles/index.css';
import '@react-pdf-viewer/toolbar/lib/styles/index.css';
import HighlightIcon from '@mui/icons-material/Highlight';
import { toolbarPlugin } from '@react-pdf-viewer/toolbar';
import { Icon } from '@react-pdf-viewer/core';

import { OnHighlightKeyword } from '@react-pdf-viewer/search';


// Import styles
import '@react-pdf-viewer/default-layout/lib/styles/index.css';

import {   HighlightArea,
    highlightPlugin,
    MessageIcon,
    RenderHighlightContentProps,
    RenderHighlightTargetProps,
    RenderHighlightsProps, } from '@react-pdf-viewer/highlight';
import '@react-pdf-viewer/highlight/lib/styles/index.css';

import { NextIcon, PreviousIcon, SearchIcon } from '@react-pdf-viewer/search'

import React, { useState } from 'react';
interface Note {
    id: number;
    content?: string;
    highlightAreas: HighlightArea[];
    quote: string;
    colour:string;
}
  const Pdf=()=>{
    //
    const [buttonmssg,setButtonmssg]=React.useState('')
    const [color,setColor]=useState("blue")
    const [message, setMessage] = React.useState('');
    const [notes, setNotes] = React.useState<Note[]>([]);
    const notesContainerRef = React.useRef<HTMLDivElement | null>(null);
    const [currentDoc, setCurrentDoc] = React.useState<PdfJs.PdfDocument | null>(null);
    let noteId = notes.length;

    const noteEles: Map<number, HTMLElement> = new Map();
    const renderHighlightTarget = (props: RenderHighlightTargetProps) => (
        <div
            style={{
                background: '#eee',
                display: 'flex',
                position: 'absolute',
                left: `${props.selectionRegion.left}%`,
                top: `${props.selectionRegion.top + props.selectionRegion.height}%`,
                transform: 'translate(0, 8px)',
                zIndex: 1,
                
            }}
        >
            <Tooltip
                position={Position.TopCenter}
                target={
                    <>
                    <Button onClick={props.toggle}>
                        <MessageIcon />
                    </Button>
                    </>
                }
                content={() => <div style={{ width: '100px' }}>Notes</div>}
                offset={{ left: 0, top: -8 }}
            />
                        <Tooltip
                position={Position.TopCenter}
                target={
                    <>
                    <Button onClick={props.toggle}>
                        <HighlightIcon />
                    </Button>
                    </>
                }
                content={() => <div style={{ width: '100px' }}>Highlights</div>}
                offset={{ left: 0, top: -8 }}
            />
        </div>
    );

    
    const renderHighlightContent = (props: RenderHighlightContentProps) => {
        const addNote = () => {
            if (message !== '') {
                const note: Note = {
                    id: ++noteId,
                   content: message,
                    highlightAreas: props.highlightAreas,
                    quote: props.selectedText,
                    colour:color
                    
                };
                console.log(note)
                setNotes(notes.concat([note]));
                props.cancel();
            }
        };

        const changeColor=(e)=>{
            console.log(e);
            setColor(e)
        }
        
       

        return (
            <div
                style={{
                    background: '#fff',
                    border: '1px solid rgba(0, 0, 0, .3)',
                    borderRadius: '2px',
                    padding: '8px',
                    position: 'absolute',
                    left: `${props.selectionRegion.left}%`,
                    top: `${props.selectionRegion.top + props.selectionRegion.height}%`,
                    zIndex: 1,
                }}
            >
                <div>
                
                    <div>
                        <div onClick={()=>changeColor("red")} style={{display:"inline",border:"2px solid gray ", padding:"2px",margin:"2px",cursor:"pointer"}}>Red</div>
                        <div onClick={()=>changeColor("yellow")}style={{width :"20",display:"inline",border:"2px solid gray ", padding:"2px",margin:"2px",cursor:"pointer"}}>Yellow</div>
                        <div onClick={()=>changeColor("blue")} style={{width :"20",display:"inline",border:"2px solid gray ", padding:"2px",margin:"2px",cursor:"pointer"}}>Blue</div>
                        <div onClick={()=>changeColor("green")} style={{width :"20",display:"inline",border:"2px solid gray ", padding:"2px",margin:"2px",cursor:"pointer"}}>Green</div>
                    </div>

                    <textarea
                        rows={3}
                        style={{
                            border: '1px solid rgba(0, 0, 0, .3)',
                        }}
                        onChange={(e) => setMessage(e.target.value)}
                    ></textarea>
                </div>
                <div
                    style={{
                        display: 'flex',
                        marginTop: '8px',
                    }}
                >
                    <div style={{ marginRight: '8px' }}>
                        <PrimaryButton onClick={addNote}>Add</PrimaryButton>
                    </div>
                    <Button onClick={props.cancel}>Cancel</Button>
                </div>
            </div>
        );
    };
    const jumpToNote = (note: Note) => {
        activateTab(3);
        const notesContainer = notesContainerRef.current;
        if (noteEles.has(note.id) && notesContainer) {
            notesContainer.scrollTop = noteEles.get(note.id).getBoundingClientRect().top;
        }
        
    };
    
    
    const renderHighlights = (props: RenderHighlightsProps) => (
        <div>
            {notes.map((note) => (
                <React.Fragment key={note.id}>
                    {note.highlightAreas
                        .filter((area) => area.pageIndex === props.pageIndex)
                        .map((area, idx) => (
                            <div
                                key={idx}
                                style={Object.assign(
                                    {},
                                    {
                                        background: note.colour,
                                        opacity: 0.4,
                                    },
                                    props.getCssProperties(area, props.rotation)
                                )}
                                onClick={() => jumpToNote(note)}
                            />
                        ))}
                </React.Fragment>
            ))}
        </div>
    );

    //
    const searchPluginInstance = searchPlugin({
        onHighlightKeyword: (props: OnHighlightKeyword) => {
            props.highlightEle.style.outline = '2px dashed blue';
            props.highlightEle.style.backgroundColor = 'rgba(0, 0, 0, .1)';
        },
    });
    const bookmarkPluginInstance = bookmarkPlugin();
    const highlightPluginInstance = highlightPlugin({
        renderHighlightTarget,
        renderHighlightContent,
        renderHighlights,
    });
    const { jumpToHighlightArea } = highlightPluginInstance;
    React.useEffect(() => {
        return () => {
            noteEles.clear();
        };
    }, []);
    const sidebarNotes = (
        <div
            ref={notesContainerRef}
            style={{
                overflow: 'auto',
                width: '100%',
            }}
        >
            {notes.length === 0 && <div style={{ textAlign: 'center' }}>There is no note</div>}
            {notes.map((note) => {
                return (
                    <div
                        key={note.id}
                        style={{
                            borderBottom: '1px solid rgba(0, 0, 0, .3)',
                            cursor: 'pointer',
                            padding: '8px',
                        }}
                        onClick={() => jumpToHighlightArea(note.highlightAreas[0])}
                        ref={(ref): void => {
                            noteEles.set(note.id, ref as HTMLElement);
                        }}
                    >
                        <blockquote
                            style={{
                                borderLeft: '2px solid rgba(0, 0, 0, 0.2)',
                                fontSize: '.75rem',
                                lineHeight: 1.5,
                                margin: '0 0 8px 0',
                                paddingLeft: '8px',
                                textAlign: 'justify',
                            }}
                        >
                            {note.quote}
                        </blockquote>
                        {note.content}
                    </div>
                );
            })}
        </div>
    );

    const defaultLayoutPluginInstance = defaultLayoutPlugin({
        setInitialTab: () => Promise.resolve(0),
        toolbarPlugin: {
            searchPlugin: {
                keyword: ['document'],
                onHighlightKeyword: (props: OnHighlightKeyword) => {
                    props.highlightEle.style.outline = '1px dashed blue';
                    props.highlightEle.style.backgroundColor = 'rgba(0, 0, 0, .1)';
                },
            },
        },
        sidebarTabs: (defaultTabs) =>
            defaultTabs.concat({
                content: sidebarNotes,
                icon: <MessageIcon />,
                title: 'NoTES',
            },{
            content: <div style={{ textAlign: 'center', width: '100%' }}>Highlights are listed here</div>,
            icon:<HighlightIcon />,
            title: 'HIGHLIGHTS',
        }),
            
        
        
    });
    const { activateTab } = defaultLayoutPluginInstance;
    const handleDocumentLoad = (e: DocumentLoadEvent) => {
        const { activateTab } = defaultLayoutPluginInstance;
        activateTab(1);
        setCurrentDoc(e.doc);
        if (currentDoc && currentDoc !== e.doc) {
            // User opens new document
            setNotes([]);
        }
    };
   
    const { Bookmarks } = bookmarkPluginInstance;
return (
    <Worker workerUrl="https://unpkg.com/pdfjs-dist@3.1.81/build/pdf.worker.min.js">
    <div
    style={{
        border: '1px solid rgba(0, 0, 0, 0.3)',
        height: '750px',
    }}
>
    
    <Viewer
    plugins={[defaultLayoutPluginInstance,bookmarkPluginInstance,searchPluginInstance,highlightPluginInstance]}
        theme={{
            theme: 'dark',
        }}
        onDocumentLoad={handleDocumentLoad} 
    // plugins={[searchPluginInstance]} 
    fileUrl="https://home.uchicago.edu/~jcarlsen/2007/downloads/Selections.pdf" />

</div>
</Worker>
)
}
export default Pdf
function activateTab(arg0: number) {
    throw new Error('Function not implemented.');
}

